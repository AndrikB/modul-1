#include "Beta.h"
#include "Red.h"
#include "Green.h"

#include<vector>

bool is_here(std::vector<Base*> tmp, Base* el)
{
	int size = tmp.size();
	for (int i = 0; i < size; i++)
	{
		if (tmp[i] == el) return true;
	}
	return false;
}

void push_el_in_V(std::vector<Base*> &tmp, Base* el)
{
	if (is_here(tmp, el)) return;
	tmp.push_back(el);
	if (el->is_b1()) push_el_in_V(tmp, el->return_B1());
	if (el->is_b2()) push_el_in_V(tmp, el->return_B2());
}

void psevdo_delete(std::vector<Base*> V)
{
	double S = 0;
	int size = V.size();
	std::vector<Base*> tmp;
	for (int i = 0; i < size; i++)
	{
		push_el_in_V(tmp, V[i]);
	}
	size = tmp.size();
	for (int i = 0; i < size; i++)
	{
		tmp[i]->simulator_destructor(S);
	}

	cout << S << endl;
}

void task1()
{
	cout << "task 1\n";
	Base *A = new Alpha;
	Base *B = new Beta;
	Base *C = new Red;
	Base *D = new Alpha;
	delete D;
	cout << S << endl;
	delete C;
	cout << S << endl;
	delete B;
	cout << S << endl;
	delete A;
	cout << S << endl;
	S = 0;
	system("pause");
}

void task2()
{
	system("cls");
	cout << "task 2\n";
	Base *A = new Alpha;
	Base *B = new Beta;
	Base *C = new Red;
	Base *D = new Alpha;
	A->set_B2(C);
	B->set_B1(D);
	std::vector<Base *> V;
	V.push_back(C);
	V.push_back(A);	
	psevdo_delete(V);
	S = 0;
	delete C;
	delete A;
	cout << S;
	system("pause");
}



void overtaking(std::vector<Base*> V)
{
	int M = V.size();
	std::vector<Base*> combiantion_el;
	if (M == 0) return;
	if (M >= 7) return;
	int count = 0;
	for (int a = 0; a < M; a++)//0
	{
		combiantion_el.push_back(V[a]);
		cout << "case " << count << ": " << a << " : \n";
		psevdo_delete(combiantion_el);
		for (int b = 0; b < M; b++)//1
		{
			if (b == a)continue;
			combiantion_el.push_back(V[b]);
			cout << "case " << count << ": " << a << ' ' << b << " : \n";
			psevdo_delete(combiantion_el);
			for (int c = 0; c < M; c++)//2
			{
				if (c == a)continue;
				if (c == b)continue;
				combiantion_el.push_back(V[c]);
				cout << "case " << count << ": " << a << ' ' << b << ' ' << c << " : \n";
				psevdo_delete(combiantion_el);

				for (int d = 0; d < M; d++)//3
				{
					if (d == a)continue;
					if (d == b)continue;
					if (d == c)continue;
					combiantion_el.push_back(V[d]);
					cout << "case " << count << ": " << a << ' ' << b << ' ' << c << ' ' << d << " : \n";
					psevdo_delete(combiantion_el);

					for (int e = 0; e < M; e++)//4
					{
						if (e == a)continue;
						if (e == b)continue;
						if (e == c)continue;
						if (e == d)continue;
						combiantion_el.push_back(V[e]);
						cout << "case " << count << ": " << a << ' ' << b << ' ' << c << ' ' << d << ' ' << e << " : \n";
						psevdo_delete(combiantion_el);

						for (int f = 0; f < M; f++)//5
						{
							if (f == a)continue;
							if (f == b)continue;
							if (f == c)continue;
							if (f == d)continue;
							if (f == e)continue;
							combiantion_el.push_back(V[f]);
							cout << "case " << count << ": " << a << ' ' << b << ' ' << c << ' ' << d << ' ' << e << ' ' << f << " : \n";
							psevdo_delete(combiantion_el);




							combiantion_el.erase(combiantion_el.begin() + 5);
						}


						combiantion_el.erase(combiantion_el.begin() + 4);
					}


					combiantion_el.erase(combiantion_el.begin() + 3);
				}


				combiantion_el.erase(combiantion_el.begin() + 2);
			}
			combiantion_el.erase(combiantion_el.begin() + 1);
		}

		combiantion_el.erase(combiantion_el.begin());
	}
	

}

void task3()
{
	system("cls");
	cout << "task 3(overtaing)\n";
	Base *a = new Alpha;
	Base *b = new Beta; b->set_B1(a);
	Base *c = new Red; 
	Base *d = new Green; d->set_B1(b); d->set_B2(c);
	Base *e = new Alpha;
	Base *g = new Red;
	std::vector<Base *> V;
	V.push_back(a);
	V.push_back(b);
	V.push_back(c);
	V.push_back(d);
	V.push_back(e);
	V.push_back(g);
	overtaking(V);
}

int main()
{
	task1();
	task2();
	task3();
	system("pause");
}