#pragma once
#include<iostream>
using std::cin;
using std::cout;
using std::endl;

double S = 0;

class Base
{
public:
	Base();
	virtual ~Base();
	virtual void simulator_destructor(double &Simulator_S);
	void set_B1(Base *B);
	void set_B2(Base *B);
	Base* return_B1() { return B1; }
	Base* return_B2() { return B2; }
	bool is_b1() { return B1 != nullptr; }
	bool is_b2() { return B2 != nullptr; }

protected:
	int index;
	Base *B1 = nullptr, *B2 = nullptr, *father = nullptr;
};

Base::Base()
{
}

Base::~Base()
{
	if (father)
	{
		if (this == father->B1)father->B1 = nullptr;
		else father->B2 = nullptr;
	}
	if (B1)delete B1;
	if (B2)delete B2;
	S = 2*S + index - 12;
	cout << "Delete Base " << index << endl;
	
}

void Base::simulator_destructor(double &Simulator_S)
{
	Simulator_S = 2*Simulator_S + index - 12;
	cout << "psevdo delete Base " << index << endl;
}

void Base::set_B1(Base *B)
{
	B1 = B;
	B1->father = this;
}

void Base::set_B2(Base *B)
{
	B2 = B;
	B2->father = this;
}

