#include "server.h"

void server::add_program( possibility_send_get sg, how_send hs, int t, bool wait_type, vector< m�ssage_type> mt)
{
	prog.push_back(new program(prog.size(),sg, hs, t, wait_type, mt));
}

void server::next_moment_of_time()
{
	if (!work) return;
	bool need_to_clear = true;
	int size_prog = prog.size();
	for (int i = 0; i < size_prog; i++)//all mess from prog to serv
	{
		int size_mes_in_prog = prog[i]->messages.size();
		for (int k = 0; k < size_mes_in_prog; k++)
		{
			prog[i]->messages[0].start_server = N;
			messages_which_was_here.push_back(prog[i]->messages[0].N);
			this->messages_in_server.push_back(prog[i]->messages[0]);
			prog[i]->messages.erase(prog[i]->messages.begin());

		}
	}


	int size_mes = messages_in_server.size();
	for (int i = 0; i < size_mes; i++)
	{
		if (messages_in_server[i].end_server == N && messages_in_server[i].proc >= 1)
		{ 
			end_server_for_mess(messages_in_server[i]); 

			if (prog[messages_in_server[i].end_program]->wait_for_message_type)
			{
				int size_mess_t = prog[messages_in_server[i].end_program]->m�ss_type.size();
				for (int z = 0; z < size_mess_t; z++)
				{
					if (messages_in_server[i].type == prog[messages_in_server[i].end_program]->m�ss_type[z])
						prog[messages_in_server[i].end_program]->wait_for_message_type = false;
				}

			}
			
			if (prog[messages_in_server[i].end_program]->when_send == SendAfterGet)
			{
				prog[messages_in_server[i].end_program]->dt = 0;
			}
			messages_in_server.erase(messages_in_server.begin() + i); 
			i--;
			size_mes--;
			continue;
		}
		if (messages_in_server[i].proc < 1) need_to_clear = false;
	}
	
	//bool b=false;
	//for (int j = 0; j < size_prog; j++)
	//	if (prog[j]->send_get !=GetOnly) b = true;
	//if (!b) messages_in_server.clear();

	for (int i = 0; i < size_prog; i++)
	{
		if (prog[i]->send_get == GetOnly && !prog[i]->wait_for_message_type)continue;
		prog[i]->dt--;
		
		if (prog[i]->dt == 0)//start program
		{
			int size_serv = neighbours.size();
			for (int k = 0; k < size_mes; k++)//k - message
			{
				if (messages_in_server[k].proc < 1)continue;

				for (int j = 0; j < size_serv; j++)//j - neighbour server
				{
					int size_mes_nei= neighbours[j].s->messages_which_was_here.size();
					bool b = false;
					for (int i = 0; i < size_mes_nei; i++)//if was mess here
						if (neighbours[j].s->messages_which_was_here[i] == messages_in_server[k].N)
						{
							b = true;
							i = size_mes_nei;
							continue;
						}
					if (!b)//if wasnt there
					{
						int index = 0;
						bool is_here = false;
						int size_is_there = neighbours[j].s->messages_in_server.size();
						for (int i = 0; i < size_is_there; i++)
						{
							if (messages_in_server[k].N == neighbours[j].s->messages_in_server[i].N)
								is_here = true;//if is here
							index = i;
						}
						if (!is_here) 
						{ 
							message m(true);
							m.N = messages_in_server[k].N;
							m.data = messages_in_server[k].data;
							m.end_program = messages_in_server[k].end_program;
							m.end_server = messages_in_server[k].end_server;
							m.proc = 0;
							m.size = messages_in_server[k].size;
							m.start_program = messages_in_server[k].start_program;
							m.start_server = messages_in_server[k].start_server;
							m.type = messages_in_server[k].type;
							neighbours[j].s->messages_in_server.push_back(m); 
							index = neighbours[j].s->messages_in_server.size() - 1; 
						}
						neighbours[j].s->messages_in_server[index].proc = neighbours[j].s->messages_in_server[index].proc + neighbours[j].speed*1.0 / messages_in_server[k].size;
						
						
						if (neighbours[j].s->messages_in_server[index].proc >= 1)
							neighbours[j].s->messages_which_was_here.push_back(messages_in_server[k].N);
						else need_to_clear = false;
					}
					
				}
			}
			if (need_to_clear) messages_in_server.clear();
			if (prog[i]->when_send == SendPeriodically) prog[i]->dt = prog[i]->dt + prog[i]->dct;
			else if (prog[i]->when_send == Send�asually) prog[i]->dt = prog[i]->dt + rand()% prog[i]->dct;
			return;
		}
		
	}

	
		
		

}

void server::end_server_for_mess(message m)
{
	cout << "		Message " << m.N << " is on "<<m.end_server<<" server\n";
	if (m.end_program<prog.size())
		this->prog[m.end_program]->input_messages.push_back(m);//LET PROGRAM THINK WHAT DOES IT WANTS TO DO WITH IT
	if (m.type == send_answer_rec && this->prog[m.end_program]->send_get != GetOnly)
	{
		this->prog[m.end_program]->send_message(m.start_server, m.start_program, m.size, send_answer_rec);
	}
	else if (m.type == send_answer && this->prog[m.end_program]->send_get != GetOnly)
	{
		this->prog[m.end_program]->send_message(m.start_server, m.start_program, m.size, information);
	}
}