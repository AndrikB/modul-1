#include "server.h"

void set_connection(int i, int j, int speed,  vector<server*> S)
{
	if (S.size() > i || S.size() > j)
	{
		neighbour_info s1, s2;
		s1.s = S[i]; s1.speed = speed;
		s2.s = S[j]; s2.speed = speed;
		S[i]->neighbours.push_back(s2);
		S[j]->neighbours.push_back(s1);
	}
}


void task1()
{
	cout << "model for 3 server; all is conected; 0 send to 1 mess every 3 sek; 2 send to 1 mess every 4 sek, and server 1 ans on them";
	vector<server*> servers;
	servers.push_back(new server());
	servers[0]->add_program(SendGet, SendPeriodically, 3);
	servers[0]->add_program(SendGet, SendPeriodically, 1);

	servers.push_back(new server());
	servers[1]->add_program(GetOnly);
	servers[1]->add_program(SendGet, SendPeriodically, 1);

	servers.push_back(new server());
	servers[2]->add_program(GetOnly);
	servers[2]->add_program(SendGet, SendPeriodically, 1);


	set_connection(0, 1, 7, servers);
	set_connection(1, 2, 5, servers);
	set_connection(0, 2, 2, servers);
	for (int i = 0; i < 20; i++)
	{
		cout << "Moment time " << i << ":\n";

		for (int j = 0; j < servers.size(); j++)
		{
			cout << "	server " << j << ":\n";
			if (i % 3 == 0 && j == 0)
			{
				servers[0]->prog[0]->send_message(1, 0, 20, send_answer_rec);
			}

			if (i % 4 == 1 && j == 2)
			{
				servers[0]->prog[1]->send_message(1, 1, 20, send_answer_rec);
			}

			servers[j]->next_moment_of_time();
		}
	}
	system("pause");
}

void task2()
{
	system("cls");
	cout << "the same system, but for virus; he started on server 0\n";
	vector<server*> servers;
	servers.push_back(new server());
	servers[0]->add_program(SendGet, SendPeriodically, 3);
	servers[0]->add_program(SendGet, SendPeriodically, 1);

	servers.push_back(new server());
	servers[1]->add_program(GetOnly);
	servers[1]->add_program(SendGet, SendPeriodically, 1);

	servers.push_back(new server());
	servers[2]->add_program(GetOnly);
	servers[2]->add_program(SendGet, SendPeriodically, 1);

	set_connection(0, 1, 7, servers);
	set_connection(1, 2, 5, servers);
	set_connection(0, 2, 2, servers);

	servers[0]->prog[1]->send_message(-1, 0, 40, virus);

	int server_size = servers.size();
	for (int i = 0; i < 7; i++)
	{
		cout << "Moment time " << i << ":\n";

		for (int j = 0; j < server_size; j++)
		{
			cout << "	server " << j << ":\n";

			servers[j]->next_moment_of_time();
		}
	}

	for (int i = 0; i < server_size; i++)
	{
		int mes_was_here = servers[i]->messages_which_was_here.size();

		for (int j = 0; j < mes_was_here; j++)
		{
			if (messages[servers[i]->messages_which_was_here[j]]->type == virus)
				servers[i]->work = false;
			cout << "server " << i << " stop working\n";
		}

	}
	

	system("pause");
}


int main()
{
	task2();
	//task2();


}