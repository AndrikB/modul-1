#pragma once
#include <type_traits>
#include <string>
#include<vector>
#include <iostream>
using std::ostream;
using std::istream;


int count_of_words(std::string st)
{
	int count = 0;
	int size = st.size();
	int start = 0;
	int i = 0;//tmp end
	while (i < size)
	{
		if (st[i] == ' ')
		{
			if (i - start <= 5 && i - start >= 3) count++;
			start = i + 1;
		}
		i++;
	}
	return count;
}

enum colour
{
	red,
	green,
	blue
};

struct pair
{
	colour col;
	int val;
};

istream &operator>>(istream &is, pair &d)
{
	int c;
	is >> c >> d.val;
	d.col = static_cast<colour>(c);
	return is;
}

ostream &operator<<(ostream &os, const pair &d)
{
	std::string s;
	if (d.col == 0) s = "red";
	else if (d.col == 1) s = "green";
	else s = "blue";
	os << s << ' ' << d.val; return os;
}

template<typename T> pair set(T data)//7
{
	pair p;
	p.col = red;
	p.val = 8892;
	return p;
}

template<> pair set<int>(int data)//1, 2
{
	pair p;
	
	if (data > 0) 
	{
		p.col = red;
		p.val = (int)(pow(2, data) + pow(data, 2)) % 112;
		return p;
	}
	else
	{
		p.col = green;
		p.val= (int)(pow(data, 5) + data - 1) % 212;
		if (p.val < 0) p.val += 212;
		return p;
	}
}

template<> pair set<double>(double data)//3
{
	pair p;
	p.col = blue;
	p.val = (int)(1 / sin(log2(data))) % 312;
	if (p.val < 0) p.val += 312;
	return p;
}

template<> pair set<std::string>(std::string data)//4
{
	pair p;
	p.col = green;
	p.val = count_of_words(data);
	return p;
}

template<> pair set<pair>(pair data)//5
{
	pair p;
	pair p1 = set(data.col);
	pair p2 = set(data.val);
	p.val = pow(p1.val, p2.val);
	int z = p1.col + p2.col;
	switch (z)
	{
	case 0: /*p1=p2=0*/p.col = red; return p;
	case 1: /*p1,p2=0,1*/ p.col = blue; return p;
	case 2: /*p1,p2=0,2||p1=p2=1*/ p.col = green; return p;
	case 3:/*p1,p2=1,2*/ p.col = red; return p;
	case 4: /*p1=p2=2*/ p.col = blue; return p;
	default:exit(1);
	}
}

template< typename T> pair set(std::vector<T> v)
{
	pair p;
	int size = v.size();
	int r = 0, b = 0, g = 0; //count of
	p.val= 0;
	std::vector<pair> f;
	for (int i = 0; i < size; i++)
	{
		pair t = set(v[i]);
		f.push_back(t);

		if (t.col == red) r++;
		else if (t.col = green) g++;
		else b++;
	}
	for (int i = 0; i < size; i++)
	{
		p.val= p.val+ f[i].val*f[size - i - 1].val;
	}
	p.val= p.val% 712;
	if (p.val< 0)p.val+= 712;

	if (r > g&&r > b) { p.col = red; return p; }//red max
	if (g > r&&g > b) { p.col = green; return p; }//green max
	if (b > r&&b > g) { p.col = blue; return p; }//blue max

	if (r > b&&g == r)//g and r max
	{
		for (int i = size - 1; i >= 0; i++)
		{
			if (f[i].col == red) { p.col = red; return p; }
			else if (f[i].col == green) { p.col = green; return p; }
		}
	}

	if (r > g&&b == r)//b and r max
	{
		for (int i = size - 1; i >= 0; i++)
		{
			if (f[i].col == red) { p.col = red; return p; }
			else if (f[i].col == blue) { p.col = blue; return p; }
		}
	}

	if (b > r&&b == g)//b and g max
	{
		for (int i = size - 1; i >= 0; i++)
		{
			if (f[i].col == green) { p.col = green; return p; }
			else if (f[i].col == blue) { p.col = blue; return p; }
		}
	}

	if (b == g && b == r)
	{
		p.col = f[size - 1].col;
		return p;
	}

	std::cout << "error wtf no one of this";
	return p;
}